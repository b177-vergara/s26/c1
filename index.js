
console.log(`odd-even checker for numbers 1-300`);

for (let x=1; x<=300; x++) {
	if (x % 2 === 0) {
		console.log(`${x} - even`);   
	}
	else {
		console.log(`${x} - odd`);
	}
};